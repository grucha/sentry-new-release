FROM alpine:3.8
RUN apk update && apk add bash curl
COPY pipe /usr/bin/

RUN curl -sL https://sentry.io/get-cli/ | bash

RUN chmod a+x /usr/bin/*.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]
